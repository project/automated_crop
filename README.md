# Automated Crop module

Provides an API for automatic cropping tools integration.

## Technical details

Initial discussion can be found on [automated crop integration](https://www.drupal.org/node/2830768).
